@extends('admin.layouts.master')

@section('styles')
@endsection

@section('content')
<div class='container p-4'>
    <span class="h4-font-size pr-3">{{ __('messages.adm_title.dbbackup') }}</span><span id="contentTitle" class="h2-font-size"></span>
    <div id="toolbar" class="form-group row">
      <div class="col-sm-9">
          <input type="text" class="form-control" id="txtFilename" name="txt" placeholder="{{ __('messages.adm_title.backup_filename') }}">
      </div>
      <button class="btn btn-info mr-1" type="button" title="Start Backup" id="btnDbBackup">
        <i class="fa fa-play mr-1" aria-hidden="true"></i>{{ __('messages.adm_button.start_backup') }}
      </button>
    </div>
</div>
{{-- End Container --}}
@endsection

@section('scripts')
    <script src="{{ asset('js/okcc.js') }}"></script>
    <script type="text/javascript">
      $(function () {
        var today = new Date();
        var date = today.getFullYear() + (today.getMonth()+1<10?'0':'') + (today.getMonth()+1)
                  + (today.getDate()<10?'0':'') + (today.getDate());
        var time = (today.getHours()<10?'0':'') + (today.getHours())
                  + (today.getMinutes()<10?'0':'') + (today.getMinutes())
                  +(today.getSeconds()<10?'0':'') + (today.getSeconds());
        var defaultFilename = 'digital-okcc_' + date + '_' + time + '.sql';
        $('#txtFilename').val(defaultFilename);
      });
      $('#btnDbBackup').click( function(e) {
          // call db backup restful api
          var url = "okcc/util/backupDB/" + $("#txtFilename").val();
          restApiCall(url, "GET", null, dbBackupSuccess, null);
      });

      function dbBackupSuccess(response) {
          $.unblockUI();
          toastr.success(response.data);
      }
    </script>

@endsection
