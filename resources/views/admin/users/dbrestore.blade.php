@extends('admin.layouts.master')

@section('styles')
@endsection

@section('content')
<div class='container p-4'>
    <span class="h4-font-size pr-3">{{ __('messages.adm_title.dbrestore') }}</span><span id="contentTitle" class="h2-font-size"></span>
    <div id="toolbar" class="form-inline">
        <div class="mr-2">
            <select id="dbFilename" class="form-control" name="filename" data-placeholder="{{ __('messages.adm_table.select_log') }}">
            </select>
        </div>
        <button class="btn btn-info mr-1" type="button" title="Start Restore" id="btnDbRestore">
            <i class="fa fa-play mr-1" aria-hidden="true"></i>{{ __('messages.adm_button.start_restore') }}
        </button>
    </div>
</div>
{{-- End Container --}}
@endsection

@section('scripts')
    <script src="{{ asset('js/okcc.js') }}"></script>
    <script type="text/javascript">
        $(function () {
          var url = "okcc/util/restorefiles";
          restApiCall(url, "GET", null, restorefilesSuccess, null);
        });

        $('#btnDbRestore').click( function(e) {
          var comboFilename = $('#dbFilename');
            // call db restore restful api
            var url = "okcc/util/restoreDB/" + comboFilename.val();
            restApiCall(url, "GET", null, dbRestoreSuccess, null);
        });

        function dbRestoreSuccess(response) {
            $.unblockUI();
            toastr.success(response.data);
        }

        function restorefilesSuccess(response) {
          var fileData = JSON.parse(response.data);
          var comboFilename = $('#dbFilename');
          comboFilename.empty();
          var html = '';
          $.each(fileData, function( item, name ) {
              html += '<option value="' + name + '">' + name  + '</option>';
          });
          comboFilename.prepend(html);
        }
    </script>

@endsection
