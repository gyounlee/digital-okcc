<?php

namespace App\Backup;

use Config;
use App\Http\Services\Log\SystemLog;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;

class BackupDatabase extends Command
{
    protected $process;
    private $fileanme = "";
    private $BACKUP_DIR = "backups";

    public function __construct($filename) {
      parent::__construct();

      date_default_timezone_set('US/Eastern');
      if (!is_dir(storage_path($this->BACKUP_DIR))) {
        mkdir(storage_path($this->BACKUP_DIR));
      }
      $this->filename = storage_path($this->BACKUP_DIR . "/" . $filename);
      $this->process = new Process(sprintf(
        'D:\xampp\mysql\bin\mysqldump --add-drop-database -u%s -p%s  --databases %s > %s',
        config('database.connections.mysql.username'),
        config('database.connections.mysql.password'),
        config('database.connections.mysql.database'),
        $this->filename
      ));
    }

    public function backup() {
      try {
        $this->process->mustRun();
        SystemLog::write(Config::get('app.admin.logDbBackup'), "Database wsd backed up [". $this->filename . "]");
        LOG::info("DB Backup " . $this->filename . " - Success");
      } catch (ProcessFailedException $exception) {
        LOG::debug("DB Backup " . $this->filename . " - Failed". $exception);
      }
    }
}
