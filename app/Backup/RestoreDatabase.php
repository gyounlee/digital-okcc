<?php

namespace App\Backup;

use Config;
use App\Http\Services\Log\SystemLog;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;

class RestoreDatabase extends Command
{
    protected $process;
    private $BACKUP_DIR = "backups";

    public function __construct() {
      parent::__construct();
    }

    public function restore($filename) {
      try {
        $this->process = new Process(sprintf(
          'D:\xampp\mysql\bin\mysql -u%s -p%s < %s',
          config('database.connections.mysql.username'),
          config('database.connections.mysql.password'),
          //$filename
          storage_path($this->BACKUP_DIR . "/" . $filename)
        ));

        $this->process->mustRun();
        SystemLog::write(Config::get('app.admin.logDbRestore'), "Database wsd restored with [". storage_path($this->BACKUP_DIR . $filename) . "]");
        LOG::info("DB Restore " . storage_path($this->BACKUP_DIR . $filename) . " - Success");
      } catch (ProcessFailedException $exception) {
        LOG::debug("DB Restore ". storage_path($this->BACKUP_DIR . $filename) . " - Failed". $exception);
      }
    }

    public function getRestoreFiles() {
      if (!is_dir(storage_path($this->BACKUP_DIR))) {
        mkdir(storage_path($this->BACKUP_DIR));
      }

      $arrFilenames = array();
      if ($dh = opendir(storage_path($this->BACKUP_DIR))) {
        while (($file = readdir($dh)) !== false) {
          if ($file != '.' && $file != '..') {
            array_push($arrFilenames, $file);
          }
        }
      }

      return $arrFilenames;
    }
}
