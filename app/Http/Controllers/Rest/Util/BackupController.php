<?php

namespace App\Http\Controllers\Rest\Util;

use App\Backup\BackupDatabase;
use App\Backup\RestoreDatabase;
use App\Http\Controllers\Rest\BaseController;
use Illuminate\Http\Request;

class BackupController extends BaseController
{

    public function __construct() {
    }

    /**
     * Backup database.
     *
     * @return \Illuminate\Http\Response
     */
    public function backupDB($filename)
    {
        $backupDb = new BackupDatabase($filename);
        $backupDb->backup();
        return $this->sendResponse('', "Bckup done.");
    }

    /**
     * Get Backup File list
     *
     * @return \Illuminate\Http\Response
     */
    public function restoreFiles()
    {
        $restoreDb = new RestoreDatabase();
        $result = $restoreDb->getRestoreFiles();
        return $this->sendResponse(json_encode($result), "Success.");
    }

    /**
     * Restore database.
     *
     * @return \Illuminate\Http\Response
     */
    public function restoreDB($filename)
    {
        $restoreDb = new RestoreDatabase();
        $restoreDb->restore($filename);
        return $this->sendResponse('', "Restore done.");
    }
}
