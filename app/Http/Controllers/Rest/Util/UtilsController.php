<?php

namespace App\Http\Controllers\Rest\Util;

use App\Member;
use App\User;
use App\Http\Controllers\Rest\BaseController;
use App\Http\Services\MemberList\CommonService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;


class UtilsController extends BaseController
{

    public function __construct() {
    }

    /**
     * Add a new user.  This is Display a listing of the search category.
     *
     * @return \Illuminate\Http\Response
     */
    public function addUser($id, $password)
    {
        $user = User::create([
            'name' => 'autouser',
            'email' => $id,
            'password' => Hash::make($password),
            'member_id' => 936, // This is gyounlee's member_id
            'privilege_id' => 7 // This is etc privilege
        ]);
        return $this->sendResponse('', "User created successfully.");
    }

    /**
     * Test SQL query
     *
     * @return \Illuminate\Http\Response
     */
    public function query($qstring)
    {
      $result = Member::query()
            ->where('first_name', 'like', '%'.$qstring.'%')->get()->count();

      LOG::debug("[REST API TEST-Query]". $result);
      return $this->sendResponse($result, 'Done');
    }
}
