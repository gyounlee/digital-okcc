<?php

namespace App\Http\Services\MemberList;
use App\Member;
use App\Family_Map;
use App\Code_Category;
use Illuminate\Support\Facades\Log;

/**
 * Service layer for handling member
 */
class MemberService
{
    function __construct() {

    }

    /**
     * Get member information
     * params: $id member ID
     *
     */
    public function getMember($id) {
        // TODO: Implement the logic to get member info

       // $memberinfo=(object)array();
        $member = Member::with(['codeByCityId','codeByProvinceId','codeByCountryId','codeByDutyId','codeByLevelId','codeByStatusId'
        ,'visits.user','member_histories','member_department_maps.codeByPositionId'])->find($id);

        $familys=$this->getFamilys($id);
        $member->familys=$familys;
        return $member;
    }

    /**
     * Retrieve the search category+code by categoryIds from DB
     */
    public function getCategoryByIds($ids) {

        $cates=Code_Category::with(['codes'=>function ($query) {
            $query->orderBy('order', 'asc');
        }])->whereIn('id',$ids)->get();

        return $cates;
    }

    public function getFamilys($id) {
        $familys = array();
        // Get the member table with primaryMembers (join member and family_maps)
        $member = Member::with(['primaryMembers'])->find($id);
        $primaryId = $id;

        if (!$member->primary) // member가 primary (세대주)가 아닐경우
        {
            // 가족관계가 없는 경우 empty array return
            if ($member->primaryMembers->count() == 0) {
              return array();
            }

            // primary member가 아닐경우는 반드시 1개의 record만 존재해야 한다.
            // 그렇지 않을 경우는 data에 문제가 있는 것임.
            // 모든 가족관계를 가져오기 위해 primary member id를 가져온다.
           $primaryId = $member->primaryMembers[0]->id;
        }

        $member = Member::with(['family_maps'=> function ($query) {
              $query->orderBy('relation_id', 'asc');
            }
            ,'family_maps.memberByParentId'
            ,'family_maps.memberByChildId'
            ,'family_maps.codeByRelationId'
            ])->find($primaryId);

        $primary = (object)array();
        $primary->id=$member->id;
        $primary->english_name = $member->english_name;
        $primary->relation_txt = 'HouseHolder';
        $primary->relation = 'HouseHolder';
        $primary->phones = CommonService::getMemberPhoneNumbers($member);
        $primary->names = CommonService::getMemberNames($member);
        array_push($familys, $primary);
        foreach($member->family_maps as $family)
        {
            $familyMember = (object)array();
            $familyMember->id           = $family->memberByChildId->id;
            $familyMember->english_name = $family->memberByChildId->english_name;
            $familyMember->relation_txt = $family->codeByRelationId->txt;
            if ($family->codeByRelationId->txt != "Self" && $family->codeByRelationId->txt != "Spouse") {
              $familyMember->relation_txt = "Child";
            } else {
              $familyMember->relation_txt = $family->codeByRelationId->txt;
            }
            $familyMember->phones = CommonService::getMemberPhoneNumbers($family->memberByChildId);
            $familyMember->names = CommonService::getMemberNames($family->memberByChildId);
            array_push($familys, $familyMember);
        }

        return $familys;
    }
}
