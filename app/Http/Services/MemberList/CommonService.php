<?php

namespace App\Http\Services\MemberList;

use App\Member;

/**
 * Service layer for common functions
 */
class CommonService
{
    /**
     * Get members include all code value
     */
    public static function getMemberListWithCodeValue() {
        $member = Member::with(['codeByStatusId', 'codeByLevelId', 'codeByDutyId', 'departmentId',
                                    'codeByCityId','codeByProvinceId','codeByCountryId']);
        return $member;
    }

    /**
     * Function to merge all phone number
     */
    public static function getMemberPhoneNumbers($member) {
      $phonenumbers = $member->tel_home . " / " . $member->tel_office . " / " . $member->tel_cell;
      return $phonenumbers;
    }

    /**
     * Function to merge english name and korean name
     */
    public static function getMemberNames($member) {
      $names = $member->english_name . " / " . $member->kor_name;
      return $names;
    }
}
