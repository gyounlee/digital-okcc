<?php

namespace App\Http\Services\MemberList;

use App\Member;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

/**
 * Service layer for handling Search request
 */
class SearchService
{
    function __construct() {

    }

    /**
     * Search the member and return the array
     * params: $search search string
     *
     * return member list
     *
     */
    public function getMemberList($search) {
        $memberList = CommonService::getMemberListWithCodeValue()
                        ->where(
                            DB::raw("CONCAT(IFNULL(first_name, ''), ' ', IFNULL(last_name, ''), ' ',
                              IFNULL(kor_name, ''))"), 'like', '%' . $search . '%')
                        ->select('*', DB::raw("CONCAT(IFNULL(first_name, ''),' ',IFNULL(last_name, '')) as eng_name"))
                        ->get();
        return $memberList;
    }
}
