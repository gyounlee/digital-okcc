<?php

namespace App\Exports;

use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection; // Use a Laravel Collection to populate the export.
use Maatwebsite\Excel\Concerns\Exportable; // Add download/store abilities right on the export class itself.
use Maatwebsite\Excel\Concerns\FromQuery; // Use an Eloquent query to populate the export.
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

use Illuminate\Support\Facades\Log;

use App\Member;
use App\Http\Services\MemberList\CommonService;

class MembersExport implements FromQuery, WithHeadings, WithMapping  {
    use Exportable;

    private $fieldName = '';
    private $fieldCode = '';
    private $searchString = '';
    private $tableSearchString = '';

    public function setSearchParams($fieldName, $fieldCode, $searchString, $tableSearchString) {
        $this->fieldName = $fieldName;
        $this->fieldCode = $fieldCode;
        $this->searchString = $searchString;
        $this->tableSearchString = $tableSearchString;
        return $this;
    }


    public function headings(): array
    {
        return [
            // Add head here.
            __('messages.memberlist.id'),
            __('messages.memberlist.first_name'),
            __('messages.memberlist.middle_name'),
            __('messages.memberlist.last_name'),
            __('messages.memberlist.kor_name'),
            __('messages.memberlist.dob'),
            __('messages.memberlist.gender'),
            __('messages.memberlist.email'),
            __('messages.memberlist.tel_home'),
            __('messages.memberlist.tel_office'),
            __('messages.memberlist.tel_cell'),
            __('messages.memberlist.household'),
            __('messages.memberlist.address'),
            __('messages.memberlist.postal_code'),
            __('messages.memberlist.city'),
            __('messages.memberlist.province'),
            __('messages.memberlist.country'),
            __('messages.memberlist.department'),
            __('messages.memberlist.position'),
            __('messages.memberlist.status'),
            __('messages.memberlist.level'),
            __('messages.memberlist.duty'),
            __('messages.memberlist.register_date'),
            __('messages.memberlist.baptism_date'),
            __('messages.memberlist.comment')

        ];
    }

    // Define the field to export
    // TODO: make it dynamic?
    public function map($member): array {
        return [
            $member->id,
            $member->first_name,
            $member->middle_name,
            $member->last_name,
            $member->kor_name,
            $member->dob,
            $member->gender,
            $member->email,
            $member->tel_home,
            $member->tel_office,
            $member->tel_cell,
            $member->primary,
            $member->address,
            $member->postal_code,
            $member->codeByCityId->txt,
            $member->codeByProvinceId->txt,
            $member->codeByCountryId->txt,
            $member->departments,
            $member->positions,
            $member->codeByStatusId->txt,
            $member->codeByLevelId->txt,
            $member->codeByDutyId->txt,
            $member->register_at,
            $member->baptism_at,
            $member->comment
            // TODO:need to add department and position
        ];
    }

    public function query() {
        $tableSearchString = $this->tableSearchString;
        $searchString = $this->searchString;
        if ($this->fieldName == '' && $searchString == '') {
            return $this->getTableSearchResult();
        } else if ($searchString != '') {
          return $this->getTableSearchResult()
                  ->where(DB::raw("CONCAT(IFNULL(first_name, ''), ' ', IFNULL(last_name, ''), ' ', IFNULL(kor_name, ''))"),
                                        'like', '%' . $searchString . '%');
        } else if ($this->fieldName == 'department') {
            $code = $this->fieldCode;
            return Member::query()->whereHas('departmentId', function($query) use ($code) {
                $query -> where('department_id', $code);
                });
        } else {
            return $this->getTableSearchResult()->where($this->fieldName, $this->fieldCode);
        }
    }

    /**
     * Search all member fields.
     */
    private function getTableSearchResult() {
      $tableSearchString = $this->tableSearchString;
      return Member::query()
              ->where(function ($query) use ($tableSearchString) {
                  return $query
                      ->whereHas('codeByCityId', function($q) use ($tableSearchString) {
                            $q -> where(DB::raw('txt'), 'like', '%'. $tableSearchString . '%');
                          })
                      ->orwhereHas('codeByProvinceId', function($q) use ($tableSearchString) {
                            $q -> where(DB::raw('txt'), 'like', '%'. $tableSearchString . '%');
                          })
                      ->orwhereHas('codeByCountryId', function($q) use ($tableSearchString) {
                            $q -> where(DB::raw('txt'), 'like', '%'. $tableSearchString . '%');
                          })
                      ->orwhereHas('codeByStatusId', function($q) use ($tableSearchString) {
                            $q -> where(DB::raw('txt'), 'like', '%'. $tableSearchString . '%');
                          })
                      ->orwhereHas('codeByLevelId', function($q) use ($tableSearchString) {
                            $q -> where(DB::raw('txt'), 'like', '%'. $tableSearchString . '%');
                          })
                      ->orwhereHas('codeByDutyId', function($q) use ($tableSearchString) {
                            $q -> where(DB::raw('txt'), 'like', '%'. $tableSearchString . '%');
                          })
                      ->orwhere(DB::raw("CONCAT(IFNULL(first_name, ''), ' ',
                          IFNULL(kor_name, ''), ' ',
                          IFNULL(dob, ''), ' ',
                          IFNULL(gender, ''), ' ',
                          IFNULL(email, ''), ' ',
                          IFNULL(tel_home, ''), ' ',
                          IFNULL(tel_office, ''), ' ',
                          IFNULL(tel_cell, ''), ' ',
                          IFNULL(address, ''), ' ',
                          IFNULL('departments', ''), ' ',
                          IFNULL('positions', ''), ' ',
                          IFNULL(postal_code, ''), ' ',
                          IFNULL(register_at, ''), ' ',
                          IFNULL(baptism_at, ''), ' ',
                          IFNULL(comment, ''))") , 'like', '%' . $tableSearchString . '%');
                      });

    }
}
